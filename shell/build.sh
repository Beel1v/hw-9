#!/bin/bash

cd ~/hw-9

docker build \
    --build-arg MODEL_PATH=$1 \
    --build-arg THRESHOLD_0_1=$2 \
    --build-arg THRESHOLD_0_2=$3 \
    -t web_app_9_img .
docker tag web_app_9_img 65.21.241.36:5050/web_app_9_img 
docker push 65.21.241.36:5050/web_app_9_img
docker stack deploy --compose-file docker-compose.yml web_app_9

cd -
